﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment_06
{
    class Output
    {
        // initiate shared outputs used by American and European options
        public double CallPrice;
        public double CallDelta;
        public double CallGamma;
        public double CallTheta;
        public double CallVega;
        public double CallRho;

        public double PutPrice;
        public double PutDelta;
        public double PutGamma;
        public double PutTheta;
        public double PutVega;
        public double PutRho;
    }
    class Program
    {
        static void Main(string[] args)
        {
            // This program uses a trinomial tree to price European and American options
            // given input values of:
            // K = Strike price per share in home currency
            // T = Tenor - time to expiration input as decimal years
            // S = Underlying price per share in home currency
            // sig = Volatility - standard deviation of the underlying input as decimal
            // r = Risk free interest rate continuously compounded input as decimal
            // div = Dividend rate continuously compounded input as decimal
            // N = Number of steps to calculate. In the interest of time this is NOT TO EXCEED 2500

            // initiate input variables
            double K;
            double T;
            double S;
            double sig;
            double r;
            double div;
            int N;

            // ask for inputs
            Console.Write("Input strike price per share in home currency: ");
            string KString = Console.ReadLine();
            K = Convert.ToDouble(KString);
            Console.Write("Input Tenor - time to expiration input as decimal years: ");
            string TString = Console.ReadLine();
            T = Convert.ToDouble(TString);
            Console.Write("Input underlying price per share in home currency: ");
            string SString = Console.ReadLine();
            S = Convert.ToDouble(SString);
            Console.Write("Input Volatility - standard deviation of the underlying input as decimal: ");
            string sigString = Console.ReadLine();
            sig = Convert.ToDouble(sigString);
            Console.Write("Input risk free interest rate continuously compounded input as decimal: ");
            string rString = Console.ReadLine();
            r = Convert.ToDouble(rString);
            Console.Write("Input dividend rate continuously compounded input as decimal: ");
            string divString = Console.ReadLine();
            div = Convert.ToDouble(divString);

            do // error handle input outside of N range and convert to int
            {
                Console.Write("Number of steps to calculate. In the interest of time this is NOT TO EXCEED 2500: ");
                string NString = Console.ReadLine();
                double NDouble = Convert.ToDouble(NString); // convert input to double
                N = Convert.ToInt16(NDouble); // convert double to int to handle cases of non-whole numbers entered
            } while (N > 2500 || N < 1);

            // run European and American calculations
            Output EuroOutput = EuroTrinomialFinal(K, T, S, sig, r, div, N);
            Output AmerOutput = AmerTrinomialFinal(K, T, S, sig, r, div, N);

            // output prices and grreks
            Console.WriteLine("========================================");
            Console.WriteLine("EUROPEAN CALL OPTION:"); // European
            Console.Write("Call Price: ");
            Console.WriteLine(EuroOutput.CallPrice);
            Console.Write("Call Delta: ");
            Console.WriteLine(EuroOutput.CallDelta);
            Console.Write("Call Gamma: ");
            Console.WriteLine(EuroOutput.CallGamma);
            Console.Write("Call Vega: ");
            Console.WriteLine(EuroOutput.CallVega);
            Console.Write("Call Theta: ");
            Console.WriteLine(EuroOutput.CallTheta);
            Console.Write("Call Rho: ");
            Console.WriteLine(EuroOutput.CallRho);
            Console.WriteLine("EUROPEAN PUT OPTION:");
            Console.Write("Put Price: ");
            Console.WriteLine(EuroOutput.PutPrice);
            Console.Write("Put Delta: ");
            Console.WriteLine(EuroOutput.PutDelta);
            Console.Write("Put Gamma: ");
            Console.WriteLine(EuroOutput.PutGamma);
            Console.Write("Put Vega: ");
            Console.WriteLine(EuroOutput.PutVega);
            Console.Write("Put Theta: ");
            Console.WriteLine(EuroOutput.PutTheta);
            Console.Write("Put Rho: ");
            Console.WriteLine(EuroOutput.PutRho);

            Console.WriteLine("========================================");
            Console.WriteLine("AMERICAN CALL OPTION:"); // switch to American
            Console.Write("Call Price: ");
            Console.WriteLine(AmerOutput.CallPrice);
            Console.Write("Call Delta: ");
            Console.WriteLine(AmerOutput.CallDelta);
            Console.Write("Call Gamma: ");
            Console.WriteLine(AmerOutput.CallGamma);
            Console.Write("Call Vega: ");
            Console.WriteLine(AmerOutput.CallVega);
            Console.Write("Call Theta: ");
            Console.WriteLine(AmerOutput.CallTheta);
            Console.Write("Call Rho: ");
            Console.WriteLine(AmerOutput.CallRho);
            Console.WriteLine("AMERICAN PUT OPTION:");
            Console.Write("Put Price: ");
            Console.WriteLine(AmerOutput.PutPrice);
            Console.Write("Put Delta: ");
            Console.WriteLine(AmerOutput.PutDelta);
            Console.Write("Put Gamma: ");
            Console.WriteLine(AmerOutput.PutGamma);
            Console.Write("Put Vega: ");
            Console.WriteLine(AmerOutput.PutVega);
            Console.Write("Put Theta: ");
            Console.WriteLine(AmerOutput.PutTheta);
            Console.Write("Put Rho: ");
            Console.WriteLine(AmerOutput.PutRho);

            Console.ReadLine(); // leave window open

        }

        static Output EuroTrinomialFinal(double K, double T, double S, double sig, double r, double div, int N) //European
        {
            // initiate small changes in Sigma and risk free insterest rate for Vega abd Rho calculatuions
            double deltaSig = 0.0001; 
            double deltaR = 0.0001;


            Output Output = EuroTrinomialPartial(K, T, S, sig, r, div, N); // run partial method to generate everything but Vega and Rho
            Output Output_PlusdeltaSig = EuroTrinomialPartial(K, T, S, sig + deltaSig, r, div, N); // C(sig + delta sig)
            Output Output_MinusdeltaSig = EuroTrinomialPartial(K, T, S, sig - deltaSig, r, div, N); // C(sig - delta sig)
            Output Output_PlusdeltaR = EuroTrinomialPartial(K, T, S, sig, r + deltaR, div, N); // C(r + delta r)
            Output Output_MinusdeltaR = EuroTrinomialPartial(K, T, S, sig, r - deltaR, div, N); // C(r - delta r)

            Output.CallVega = (Output_PlusdeltaSig.CallPrice - Output_MinusdeltaSig.CallPrice) / (2 * deltaSig); // calculate call vega
            Output.PutVega = (Output_PlusdeltaSig.PutPrice - Output_MinusdeltaSig.PutPrice) / (2 * deltaSig); // calculate put vega

            Output.CallRho = (Output_PlusdeltaR.CallPrice - Output_MinusdeltaR.CallPrice) / (2 * deltaR); // calculate call vega
            Output.PutRho = (Output_PlusdeltaR.PutPrice - Output_MinusdeltaR.PutPrice) / (2 * deltaSig); // calculate put vega

            return Output;
        }
        
        static Output EuroTrinomialPartial(double K, double T, double S, double sig, double r, double div, int N) // European trinomial tree
        {
            Output Output = new Output(); // initiate new Output for storing and sending out shared outputs
            //precompute constants
            double dt = T / N;
            double dx = sig * Math.Sqrt(3.0 * dt);
            double nu = r - div - 0.5 * sig * sig;
            double edx = Math.Exp(dx);
            double pu = 0.5 * ((((sig * sig * dt) + (nu * nu * dt * dt)) / (dx * dx)) + (nu * dt / dx));
            double pm = 1.0 - ((sig * sig * dt) + (nu * nu * dt * dt)) / (dx * dx);
            double pd = 0.5 * ((((sig * sig * dt) + (nu * nu * dt * dt)) / (dx * dx)) - (nu * dt / dx));
            double disc = Math.Exp(-r * dt);

            // initialise price jagged arrays - empty columns at first with an extra to account for [0][0] coordinate
            double[][] AssetPriceArray = new double[N + 1][]; 
            double[][] CallPriceArray = new double[N + 1][];
            double[][] PutPriceArray = new double[N + 1][];

            for (int i = 0; i < N + 1; i++) // run through each column
            {
                AssetPriceArray[i] = new double[2 * i + 1]; // add number of rows at each column
                CallPriceArray[i] = new double[2 * i + 1];
                PutPriceArray[i] = new double[2 * i + 1];
            }

            for (int i = 0; i < AssetPriceArray.Length; i++) // run through each column
            {
                if (i == 0) // first column which we know is a 1X1 array
                {
                    AssetPriceArray[0][0] = S; // initial position of tree
                }
                else
                {
                    for (int j = 0; j < AssetPriceArray[i].Length; j++) // run through each row and fill in values. We know that everything will be copied over from previous column except top and bottom row
                    {
                        if (j == 0) // bottom row - calculate step down
                        {
                            AssetPriceArray[i][j] = AssetPriceArray[i - 1][j] / edx;
                        }
                        else if (j == AssetPriceArray[i].Length - 1) // top row accounting for starting index value of 0 - calculate step up
                        {
                            AssetPriceArray[i][j] = AssetPriceArray[i - 1][j - 2] * edx;
                        }
                        else // the rest are lateral steps - copy value over and account for change in index position
                        {
                            AssetPriceArray[i][j] = AssetPriceArray[i - 1][j - 1];
                        }

                    }
                }

            }

            //determine intrinsic call price at last column
            for (int i = 0; i < 2 * N + 1; i++)
            {
                CallPriceArray[N][i] = Math.Max(AssetPriceArray[N][i] - K, 0.0);
            }

            for (int i = N - 1; i >= 0; i--) // step through columns starting at second to last column as intrinsic values currently reside
            {
                for (int j = 0; j < CallPriceArray[i].Length; j++) // step through rows and calculate discounted values off preceeding column
                {
                    CallPriceArray[i][j] = disc * (pu * CallPriceArray[i + 1][j + 2] + pm * CallPriceArray[i + 1][j + 1] + pd * CallPriceArray[i + 1][j]);
                }
            }

            //determine intrinsic put price at last column
            for (int i = 0; i < 2 * N + 1; i++)
            {
                PutPriceArray[N][i] = Math.Max(K - AssetPriceArray[N][i], 0.0);
            }

            for (int i = N - 1; i >= 0; i--) // step through columns starting at second to last column as intrinsic values currently reside
            {
                for (int j = 0; j < PutPriceArray[i].Length; j++) // step through rows and calculate discounted values off preceeding column
                {
                    PutPriceArray[i][j] = disc * (pu * PutPriceArray[i + 1][j + 2] + pm * PutPriceArray[i + 1][j + 1] + pd * PutPriceArray[i + 1][j]);
                }
            }
            //call greeks not dependent upon a delta of an input
            Output.CallDelta = (CallPriceArray[1][2] - CallPriceArray[1][0]) / (AssetPriceArray[1][2] - AssetPriceArray[1][0]);
            Output.CallGamma = (((CallPriceArray[1][2] - CallPriceArray[1][1]) / (AssetPriceArray[1][2] - AssetPriceArray[1][1])) - ((CallPriceArray[1][1] - CallPriceArray[1][0]) / (AssetPriceArray[1][1] - AssetPriceArray[1][0]))) / (0.5 * (AssetPriceArray[1][2] - AssetPriceArray[1][0]));
            Output.CallTheta = (CallPriceArray[0][0] - CallPriceArray[1][1]) / (dt); // derived by calculating change in price given a small change in time to approximate partial derivative WRT T

            //put greeks not dependent upon a delta of an input
            Output.PutDelta = (PutPriceArray[1][2] - PutPriceArray[1][0]) / (AssetPriceArray[1][2] - AssetPriceArray[1][0]);
            Output.PutGamma = (((PutPriceArray[1][2] - PutPriceArray[1][1]) / (AssetPriceArray[1][2] - AssetPriceArray[1][1])) - ((PutPriceArray[1][1] - PutPriceArray[1][0]) / (AssetPriceArray[1][1] - AssetPriceArray[1][0]))) / (0.5 * (AssetPriceArray[1][2] - AssetPriceArray[1][0]));
            Output.PutTheta = (PutPriceArray[0][0] - PutPriceArray[1][1]) / (dt); // derived by calculating change in price given a small change in time to approximate partial derivative WRT T

            Output.CallPrice = CallPriceArray[0][0]; // resulting call price after discounting
            Output.PutPrice = PutPriceArray[0][0]; // resulting put price after discounting

            return Output; // sends out stored Output class variables
        }

        static Output AmerTrinomialFinal(double K, double T, double S, double sig, double r, double div, int N) // American
        {
            // initiate small changes in Sigma and risk free insterest rate for Vega abd Rho calculatuions
            double deltaSig = 0.0001;
            double deltaR = 0.0001;

            Output Output = AmerTrinomialPartial(K, T, S, sig, r, div, N); // run partial method to generate everything but Vega and Rho
            Output Output_PlusdeltaSig = AmerTrinomialPartial(K, T, S, sig + deltaSig, r, div, N); // C(sig + delta sig)
            Output Output_MinusdeltaSig = AmerTrinomialPartial(K, T, S, sig - deltaSig, r, div, N); // C(sig - delta sig)
            Output Output_PlusdeltaR = AmerTrinomialPartial(K, T, S, sig, r + deltaR, div, N); // C(r + delta r)
            Output Output_MinusdeltaR = AmerTrinomialPartial(K, T, S, sig, r - deltaR, div, N);// C(r - delta r)

            Output.CallVega = (Output_PlusdeltaSig.CallPrice - Output_MinusdeltaSig.CallPrice) / (2 * deltaSig); // calculate call vega
            Output.PutVega = (Output_PlusdeltaSig.PutPrice - Output_MinusdeltaSig.PutPrice) / (2 * deltaSig); // calculate put vega

            Output.CallRho = (Output_PlusdeltaR.CallPrice - Output_MinusdeltaR.CallPrice) / (2 * deltaR); // calculate call vega
            Output.PutRho = (Output_PlusdeltaR.PutPrice - Output_MinusdeltaR.PutPrice) / (2 * deltaSig); // calculate put vega

            return Output;
        }
        
        static Output AmerTrinomialPartial(double K, double T, double S, double sig, double r, double div, int N) // American trinomial tree
        {
            Output Output = new Output(); // initiate new Output for storing and sending out shared outputs
            //precompute constants
            double dt = T / N;
            double dx = sig * Math.Sqrt(3.0 * dt);
            double nu = r - div - 0.5 * sig * sig;
            double edx = Math.Exp(dx);
            double pu = 0.5 * ((((sig * sig * dt) + (nu * nu * dt * dt)) / (dx * dx)) + (nu * dt / dx));
            double pm = 1.0 - ((sig * sig * dt) + (nu * nu * dt * dt)) / (dx * dx);
            double pd = 0.5 * ((((sig * sig * dt) + (nu * nu * dt * dt)) / (dx * dx)) - (nu * dt / dx));
            double disc = Math.Exp(-r * dt);

            // initialise price jagged arrays - empty columns at first with an extra to account for [0][0] coordinate
            double[][] AssetPriceArray = new double[N + 1][];
            double[][] CallPriceArray = new double[N + 1][];
            double[][] PutPriceArray = new double[N + 1][];

            for (int i = 0; i < N + 1; i++) // run through each column
            {
                AssetPriceArray[i] = new double[2 * i + 1]; // add number of rows at each column
                CallPriceArray[i] = new double[2 * i + 1];
                PutPriceArray[i] = new double[2 * i + 1];
            }

            for (int i = 0; i < AssetPriceArray.Length; i++) // run through each column
            {
                if (i == 0) // first column which we know is a 1X1 array
                {
                    AssetPriceArray[0][0] = S; // initial position of tree
                }
                else
                {
                    for (int j = 0; j < AssetPriceArray[i].Length; j++) // run through each row and fill in values. We know that everything will be copied over from previous column except top and bottom row
                    { 
                        if (j == 0) // bottom row - calculate step down
                        {
                            AssetPriceArray[i][j] = AssetPriceArray[i - 1][j] / edx;
                        }
                        else if (j == AssetPriceArray[i].Length - 1) // top row accounting for starting index value of 0 - calculate step up
                        {
                            AssetPriceArray[i][j] = AssetPriceArray[i - 1][j - 2] * edx;
                        }
                        else // the rest are lateral steps - copy value over and account for change in index position
                        {
                            AssetPriceArray[i][j] = AssetPriceArray[i - 1][j - 1];
                        }

                    }
                }

            }

            //determine intrinsic call price at last column
            for (int i = 0; i < 2 * N + 1; i++)
            {
                CallPriceArray[N][i] = Math.Max(AssetPriceArray[N][i] - K, 0.0);
            }

            for (int i = N - 1; i >= 0; i--) // step backward through columns starting at second to last column as intrinsic values currently reside 
            {
                for (int j = 0; j < CallPriceArray[i].Length; j++) // step through rows 
                {
                    CallPriceArray[i][j] = disc * (pu * CallPriceArray[i + 1][j + 2] + pm * CallPriceArray[i + 1][j + 1] + pd * CallPriceArray[i + 1][j]); // calculate discounted values off preceeding column
                    CallPriceArray[i][j] = Math.Max(AssetPriceArray[i][j] - K, CallPriceArray[i][j]); // overwrite with Max(intrinsic value, discounted value)
                }
            }

            //determine intrinsic put price at last column
            for (int i = 0; i < 2 * N + 1; i++)
            {
                PutPriceArray[N][i] = Math.Max(K - AssetPriceArray[N][i], 0.0);
            }

            for (int i = N - 1; i >= 0; i--) // step backward through columns starting at second to last column as intrinsic values currently reside
            {
                for (int j = 0; j < PutPriceArray[i].Length; j++)
                {
                    PutPriceArray[i][j] = disc * (pu * PutPriceArray[i + 1][j + 2] + pm * PutPriceArray[i + 1][j + 1] + pd * PutPriceArray[i + 1][j]); // calculate discounted values off preceeding column
                    PutPriceArray[i][j] = Math.Max(K - AssetPriceArray[i][j], PutPriceArray[i][j]); // overwrite with Max(intrinsic value, discounted value)
                }
            }
            //call greeks not dependent upon a delta of an input
            Output.CallDelta = (CallPriceArray[1][2] - CallPriceArray[1][0]) / (AssetPriceArray[1][2] - AssetPriceArray[1][0]);
            Output.CallGamma = (((CallPriceArray[1][2] - CallPriceArray[1][1]) / (AssetPriceArray[1][2] - AssetPriceArray[1][1])) - ((CallPriceArray[1][1] - CallPriceArray[1][0]) / (AssetPriceArray[1][1] - AssetPriceArray[1][0]))) / (0.5 * (AssetPriceArray[1][2] - AssetPriceArray[1][0]));
            Output.CallTheta = (CallPriceArray[0][0] - CallPriceArray[1][1]) / (dt); // derived by calculating change in price given a small change in time to approximate partial derivative WRT T

            //put greeks not dependent upon a delta of an input
            Output.PutDelta = (PutPriceArray[1][2] - PutPriceArray[1][0]) / (AssetPriceArray[1][2] - AssetPriceArray[1][0]);
            Output.PutGamma = (((PutPriceArray[1][2] - PutPriceArray[1][1]) / (AssetPriceArray[1][2] - AssetPriceArray[1][1])) - ((PutPriceArray[1][1] - PutPriceArray[1][0]) / (AssetPriceArray[1][1] - AssetPriceArray[1][0]))) / (0.5 * (AssetPriceArray[1][2] - AssetPriceArray[1][0]));
            Output.PutTheta = (PutPriceArray[0][0] - PutPriceArray[1][1]) / (dt); // derived by calculating change in price given a small change in time to approximate partial derivative WRT T

            Output.CallPrice = CallPriceArray[0][0]; // resulting call price after stepping back through jagged array and determining Max(intrinsic value, discounted value)
            Output.PutPrice = PutPriceArray[0][0]; // resulting put price after stepping back through jagged array and determining Max(intrinsic value, discounted value)

            return Output; // sends out stored Output class variables
        }

    }
}
